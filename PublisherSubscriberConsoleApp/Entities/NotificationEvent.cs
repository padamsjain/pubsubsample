﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublisherSubscriberConsoleApp
{
    public class NotificationEvent
    {

        public string NotificationMessage { get; private set; }

        public DateTime NotificationDate { get; private set; }

        public int value { get; private set; }

        public NotificationEvent(DateTime ADateTime, string AMessage, int AValue)
        {
            NotificationDate = ADateTime;
            NotificationMessage = AMessage;
            value = AValue;
        }

    }
}
