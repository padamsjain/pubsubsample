﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublisherSubscriberConsoleApp
{
    interface ISubscriber
    {
        void Subscribe(Publisher p);
        
        void Unsubscribe(Publisher p);

        void OnNotificationReceived(Publisher p, NotificationEvent e);

    }
}
