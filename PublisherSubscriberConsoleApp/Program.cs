﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublisherSubscriberConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // Creating different publisher instances
            Publisher APMSubscriber = new Publisher("A.P. Møller", 10000);
            Publisher SiteCoreSubscriber = new Publisher("Sitecore", 500);

            //Create different Subscriber instances
            Subscriber sub1 = new Subscriber("Padam");

            Subscriber sub2 = new Subscriber("Emilie");

            Subscriber sub3 = new Subscriber("Thomas");


            //Pass the publisher obj to their Subscribe function
            sub1.Subscribe(APMSubscriber);
            sub3.Subscribe(APMSubscriber);

            sub1.Subscribe(SiteCoreSubscriber);
            sub2.Subscribe(SiteCoreSubscriber);

            sub1.Unsubscribe(APMSubscriber);

            // run multiple pub threads for continuous notification update.
            Task task1 = Task.Factory.StartNew(() => APMSubscriber.Publish());
            Task task2 = Task.Factory.StartNew(() => SiteCoreSubscriber.Publish());
            Task.WaitAll(task1, task2);
        }
    }
}
