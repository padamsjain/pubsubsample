﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PublisherSubscriberConsoleApp
{

    class Publisher : IPublisher
    {

    //name of publisher
    public string PublisherName { get; private set; }

    //interval after which the notification should be sent
    public int Interval { get; private set; }

    //delegate function
    public delegate void Notify(Publisher p, NotificationEvent e);

    //event of the delegate function
    public event Notify OnPublish;

    public Publisher(string APublisherName, int AInterval)
    {
        PublisherName = APublisherName;
        Interval = AInterval;
    }

    //publish from here
    public void Publish()
    {
        while (true)
        {
            Thread.Sleep(Interval);
            if (OnPublish != null)
            {
                Random rnd = new Random();
                NotificationEvent notificationObj = new NotificationEvent(DateTime.Now, "New message arrived from", rnd.Next(1,100));
                OnPublish(this, notificationObj);
            }
            Thread.Yield();
        }
    }
}
}
