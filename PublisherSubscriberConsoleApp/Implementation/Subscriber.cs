﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublisherSubscriberConsoleApp
{
    class Subscriber : ISubscriber
    {
        public string SubscriberName { get; private set; }

        public Subscriber(string ASubscriberName)
        {
            SubscriberName = ASubscriberName;
        }

        // This function subscribe to the events of the publisher
        public void Subscribe(Publisher p)
        {

            // register notifications
            p.OnPublish += OnNotificationReceived; 

        }

        // This function unsubscribe from the events of the publisher
        public void Unsubscribe(Publisher p)
        {

            // unregister notifications
            p.OnPublish -= OnNotificationReceived; 
        }

        // It get executed when the event published by the Publisher
        public void OnNotificationReceived(Publisher p, NotificationEvent e)
        {
            string str;
            str = e.NotificationDate +  " Hello " + SubscriberName + ", " + e.NotificationMessage + " " + p.PublisherName +  " ," + " Rate: " + e.value; 
            Console.WriteLine(str);
        }
    }
}
